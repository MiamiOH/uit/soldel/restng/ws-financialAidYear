<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 7/23/18
 * Time: 1:29 PM
 */

namespace MiamiOH\FinancialAidYearWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class FinancialAidYearResourceProvider extends ResourceProvider
{
    private $serviceName = 'FinancialAidYear';
    private $tag = "financialAidYear";
    private $resourceRoot = "financialAidYear.v1";
    private $patternRoot = "/financialAidYear/v1";
    private $classPath = 'MiamiOH\FinancialAidYearWebService\Services\FinancialAidYearService';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => $this->tag,
            'description' => 'Banner Financial Aid Year'
        ));

        $this->addDefinition(array(
            'name' => 'FinancialAidYear',
            'type' => 'object',
            'properties' => array(
                'aidYearId' => array(
                    'type' => 'string',
                ),
                'name' => array(
                    'type' => 'string',
                ),
                'startDate' => array(
                    'type' => 'string',
                ),
                'endDate' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'FinancialAidYear.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/FinancialAidYear'
            )
        ));

        $this->addDefinition(array(
            'name' => 'FinancialAidYear.Exception',
            'type' => 'object',
            'properties' => array(
                'message' => array(
                    'type' => 'string',
                ),
                'line' => array(
                    'type' => 'integer',
                ),
                'file' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'FinancialAidYear.Exception.404',
            'type' => 'object',
            'properties' => array()
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'This service provides resources about Financial Aid Year.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                )
            ),
        ));
    }

    public function registerResources(): void
    {

        $this->addResource(array(
            'action' => 'read',
            'name' => $this->resourceRoot . '.current',
            'description' => 'Get current finaicial aid year.',
            'summary' => 'Get current finaicial aid year',
            'pattern' => $this->patternRoot . '/current',
            'service' => $this->serviceName,
            'tags' => [$this->tag],
            'method' => 'getCurrentFinaicialAidYear',
            'returnType' => 'model',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Current financial aid year',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/FinancialAidYear',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'There is no current financial aid year',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/FinancialAidYear.Exception.404',
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => $this->resourceRoot,
            'description' => 'Get a collection of financial aid years.',
            'summary' => 'Get a collection of all financial aid years',
            'pattern' => $this->patternRoot . '',
            'service' => $this->serviceName,
            'tags' => [$this->tag],
            'method' => 'getFinancialAidYearsCollection',
            'returnType' => 'collection',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Financial aid years',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/FinancialAidYear',
                    )
                )
            )
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}
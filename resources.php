<?php

return [
    'resources' => [
        'financialAidYear' => [
            MiamiOH\FinancialAidYearWebService\Resources\FinancialAidYearResourceProvider::class,
        ]
    ]
];

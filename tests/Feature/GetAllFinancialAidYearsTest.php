<?php
/**
 * Created by PhpStorm.
 * User: katukuar
 * Date: 7/24/18
 * Time: 1:10 PM
 */

namespace MiamiOH\FinancialAidYearWebService\Tests\Feature;


use MiamiOH\RESTng\App;

class GetAllFinancialAidYearsTest extends TestCase
{
    public function testGetAllFinancialAidYears()
    {
        $this->dbh->method('queryall_array')
            ->willReturn([
                [
                    'robinst_aidy_code' => '1819',
                    'robinst_aidy_desc' => 'Fall 2018 through Summer 2019',
                    'robinst_aidy_start_date' => '01-JUL-2018',
                    'robinst_aidy_end_date' => '30-JUN-2019'
                ],
                [
                    'robinst_aidy_code' => '1818',
                    'robinst_aidy_desc' => 'Fall 2017 through Summer 2018',
                    'robinst_aidy_start_date' => '01-JUL-2017',
                    'robinst_aidy_end_date' => '30-JUN-2018'
                ],
                [
                    'robinst_aidy_code' => '1817',
                    'robinst_aidy_desc' => 'Fall 2016 through Summer 2017',
                    'robinst_aidy_start_date' => '01-JUL-2016',
                    'robinst_aidy_end_date' => '30-JUN-2017'
                ],
                [
                    'robinst_aidy_code' => '1816',
                    'robinst_aidy_desc' => 'Fall 2015 through Summer 2016',
                    'robinst_aidy_start_date' => '01-JUL-2015',
                    'robinst_aidy_end_date' => '30-JUN-2016'
                ]
            ]);
        $response = $this->getJson('/financialAidYear/v1');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'aidYearId' => '1819',
                    'name' => 'Fall 2018 through Summer 2019',
                    'startDate' => '2018-07-01',
                    'endDate' => '2019-06-30'
                ],
                [
                    'aidYearId' => '1818',
                    'name' => 'Fall 2017 through Summer 2018',
                    'startDate' => '2017-07-01',
                    'endDate' => '2018-06-30'
                ],
                [
                    'aidYearId' => '1817',
                    'name' => 'Fall 2016 through Summer 2017',
                    'startDate' => '2016-07-01',
                    'endDate' => '2017-06-30'
                ],
                [
                    'aidYearId' => '1816',
                    'name' => 'Fall 2015 through Summer 2016',
                    'startDate' => '2015-07-01',
                    'endDate' => '2016-06-30'
                ],
            ]
        ]);


    }
}
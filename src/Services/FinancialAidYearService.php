<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 7/23/18
 * Time: 1:30 PM
 */

namespace MiamiOH\FinancialAidYearWebService\Services;

use Carbon\Carbon;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;

class FinancialAidYearService extends Service
{
    private $database = '';
    private $datasource_name = 'MUWS_GEN_PROD'; // secure datasource

    /************************************************/
    /**********Setter Dependency Injection***********/
    /***********************************************/

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database->getHandle($this->datasource_name);
    }

    public function getCurrentFinaicialAidYear()
    {

        $response = $this->getResponse();

        $whereString = ' where ROBINST_AIDY_end_date > sysdate';
        $results = $this->database->queryall_array($this->buildQueryFinancialAidYear($whereString));

        if (sizeof($results) <= 0) {
            $response->setStatus(App::API_NOTFOUND);
            return $response;
        }
        $payload = $this->buildRecord($results[0]);
        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    public function getFinancialAidYearsCollection()
    {
        $response = $this->getResponse();

        $whereString = '';
        $results = $this->database->queryall_array($this->buildQueryFinancialAidYear($whereString));

        foreach ($results as $row) {
            $payload[] = $this->buildRecord($row);
        }
        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;

    }

    private function buildQueryFinancialAidYear($whereString)
    {
        $queryString = "select robinst_aidy_code,
                          robinst_aidy_desc,
                          robinst_aidy_start_date,
                          robinst_aidy_end_date
		from robinst " . $whereString . " order by robinst_aidy_start_date DESC";

        return $queryString;
    }

    private function buildRecord($row)
    {
        $record = array(
            'aidYearId' => $row['robinst_aidy_code'],
            'name' => $row['robinst_aidy_desc'],
            'startDate' => date('Y-m-d', strtotime($row['robinst_aidy_start_date'])),
            'endDate' => date('Y-m-d', strtotime($row['robinst_aidy_end_date']))
        );

        return $record;
    }

}
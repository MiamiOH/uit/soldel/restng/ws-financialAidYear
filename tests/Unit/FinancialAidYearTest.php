<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 7/23/18
 * Time: 1:31 PM
 */

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Legacy\DB\DBH;
use PHPUnit\Framework\TestCase;
use \MiamiOH\FinancialAidYearWebService\Services\FinancialAidYearService;

class FinancialAidYearServiceTest extends TestCase
{
    /**
     * @var FinancialAidYearService
     */
    private $financialAidYearService;

    /**
     * @var array
     */
    private $mockCurrentFinancialAidYear;

    private $request;
    private $dbh;

    protected function setUp()
    {
        //set up the mock request:
        $this->request = $this->createMock(Request::class);

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder(DBH::class)
            ->setMethods(['getResourceParamKey', 'queryall_array'])
            ->getMock();

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the service with the mocked out resources:
        $this->financialAidYearService = new FinancialAidYearService();
        $this->financialAidYearService->setDatabase($db);
        $this->financialAidYearService->setRequest($this->request);
    }

    public function mockQueryCurrentFinancialAidYear()
    {
        return array(
            array(
                'robinst_aidy_code' => '1819',
                'robinst_aidy_desc' => 'Fall 2018 through Summer 2019',
                'robinst_aidy_start_date' => '01-JUL-2018',
                'robinst_aidy_end_date' => '30-JUN-2019'
            )
        );
    }

    public function mockQueryCurrentFinancialAidYearPast()
    {
        return array(
            'robinst_aidy_code' => '1818',
            'robinst_aidy_desc' => 'Fall 2017 through Summer 2018',
            'robinst_aidy_start_date' => '01-JUL-2017',
            'robinst_aidy_end_date' => '30-JUN-2018'
        );
    }

    public function mockQueryAllFinancialAidYears()
    {
        return array(
            array(
                'robinst_aidy_code' => '1819',
                'robinst_aidy_desc' => 'Fall 2018 through Summer 2019',
                'robinst_aidy_start_date' => '01-JUL-2018',
                'robinst_aidy_end_date' => '30-JUN-2019'
            ),
            array(
                'robinst_aidy_code' => '1818',
                'robinst_aidy_desc' => 'Fall 2017 through Summer 2018',
                'robinst_aidy_start_date' => '01-JUL-2017',
                'robinst_aidy_end_date' => '30-JUN-2018'
            ),
            array(
                'robinst_aidy_code' => '1817',
                'robinst_aidy_desc' => 'Fall 2016 through Summer 2017',
                'robinst_aidy_start_date' => '01-JUL-2016',
                'robinst_aidy_end_date' => '30-JUN-2017'
            ),
            array(
                'robinst_aidy_code' => '1816',
                'robinst_aidy_desc' => 'Fall 2015 through Summer 2016',
                'robinst_aidy_start_date' => '01-JUL-2015',
                'robinst_aidy_end_date' => '30-JUN-2016'
            )
        );
    }

    public function mockExpectedAllFinancialAidYearResponse()
    {
        $expectedReturn = array(
            array(
                'aidYearId' => '1819',
                'name' => 'Fall 2018 through Summer 2019',
                'startDate' => '2018-07-01',
                'endDate' => '2019-06-30'
            ),
            array(
                'aidYearId' => '1818',
                'name' => 'Fall 2017 through Summer 2018',
                'startDate' => '2017-07-01',
                'endDate' => '2018-06-30'
            ),
            array(
                'aidYearId' => '1817',
                'name' => 'Fall 2016 through Summer 2017',
                'startDate' => '2016-07-01',
                'endDate' => '2017-06-30'
            ),
            array(
                'aidYearId' => '1816',
                'name' => 'Fall 2015 through Summer 2016',
                'startDate' => '2015-07-01',
                'endDate' => '2016-06-30'
            ),
        );

        return $expectedReturn;
    }

    public function mockExpectedCurrentFinancialAidYearsResponse()
    {
        return array(
            'aidYearId' => '1819',
            'name' => 'Fall 2018 through Summer 2019',
            'startDate' => '2018-07-01',
            'endDate' => '2019-06-30'
        );
    }

    public function testGetCurrentFinancialAidYearWillReturnCurrentFinancialAidYear(): void
    {

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryCurrentFinancialAidYear')));

        $response = $this->financialAidYearService->getCurrentFinaicialAidYear();
        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals($this->mockExpectedCurrentFinancialAidYearsResponse(), $response->getPayload());
    }

    public function mockNoDataQuery()
    {
        return array();
    }

    public function testGetCurrentFinancialAidYearWillReturnEmptyRecord(): void
    {
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockNoDataQuery')));

        $response = $this->financialAidYearService->getCurrentFinaicialAidYear();
        $this->assertEquals(App::API_NOTFOUND, $response->getStatus());
    }

    public function testGetAllFinancialAidYearWillReturnAllFinancialAidYears(): void
    {
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllFinancialAidYears')));

        $response = $this->financialAidYearService->getFinancialAidYearsCollection();
        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals($this->mockExpectedAllFinancialAidYearResponse(), $response->getPayload());
    }


}
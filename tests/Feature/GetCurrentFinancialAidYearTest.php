<?php
/**
 * Created by PhpStorm.
 * User: katukuar
 * Date: 7/24/18
 * Time: 1:10 PM
 */

namespace MiamiOH\FinancialAidYearWebService\Tests\Feature;


use MiamiOH\RESTng\App;

class GetCurrentFinancialAidYearTest extends TestCase
{
    public function testGetCurrentFinancialAidYears()
    {
        $this->dbh->method('queryall_array')
            ->willReturn([
                [
                    'robinst_aidy_code' => '1819',
                    'robinst_aidy_desc' => 'Fall 2018 through Summer 2019',
                    'robinst_aidy_start_date' => '01-JUL-2018',
                    'robinst_aidy_end_date' => '30-JUN-2019'
                ]
            ]);
        $response = $this->getJson('/financialAidYear/v1/current');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'aidYearId' => '1819',
                'name' => 'Fall 2018 through Summer 2019',
                'startDate' => '2018-07-01',
                'endDate' => '2019-06-30'
            ]
        ]);


    }

    public function testCurrentFinancialAidYearNotFound() {
        $this->dbh->method('queryall_array')
            ->willReturn([]);

        $response = $this->getJson('/financialAidYear/v1/current');

        $response->assertStatus(App::API_NOTFOUND);
        $response->assertJson([
            'data' => []
        ]);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: katukuar
 * Date: 7/24/18
 * Time: 1:10 PM
 */

namespace MiamiOH\FinancialAidYearWebService\Tests\Feature;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use PHPUnit\Framework\MockObject\MockObject;

class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    protected $dbh;

    protected function setUp()
    {
        parent::setUp();
        $databaseFactory = $this->createMock(DatabaseFactory::class);
        $this->dbh = $this->createMock(DBH::class);
        $databaseFactory->method('getHandle')->willReturn($this->dbh);

        $this->app->useService([
            'name' => 'APIDatabaseFactory',
            'object' => $databaseFactory,
            'description' => 'Mocked Database Factory'
        ]);
    }

}
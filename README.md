# Financial Aid Year Web Service

Financial aid year web service(s) provides current financial aid year information (e.g, aid year id , name, start date , end date) in the Miami University.

Financial aid year web service was originally written in RestServer.

### Resources:

#### GET GET /financialAidYear/v1/current

This resource is used to retrieve a [financialAidYear](./docs/models/financialAidYear.MD) for current year.

#### GET GET /financialAidYear/v1

This resource is used to retrieve a collection of [financialAidYear](./docs/models/financialAidYear.MD).